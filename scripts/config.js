import path from 'path'
import vue from 'rollup-plugin-vue'
import babel from 'rollup-plugin-babel'
import replace from 'rollup-plugin-replace'
import scss from 'rollup-plugin-scss'
import serve from 'rollup-plugin-serve'
import commonjs from 'rollup-plugin-commonjs'
import livereload from 'rollup-plugin-livereload'
import { uglify } from 'rollup-plugin-uglify'
import resolve from 'rollup-plugin-node-resolve'

const { author, version } = require('../package.json')

const banner =
  '/*!\n' +
  ' * Gitee-Frontend.js v' + (process.env.VERSION || version) + '\n' +
  ' * (c) ' + new Date().getFullYear() + ' ' + author + '\n' +
  ' * Released under the MIT License.\n' +
  ' */'


function resolvePath(p) {
  return path.resolve(__dirname, '../', p || '')
}

const builds = {
  // Runtime only (CommonJS). Used by bundlers e.g. Webpack & Browserify
  'main-cjs': {
    entry: resolvePath('src/gitee-frontend.js'),
    dest: resolvePath('dist/gitee-frontend.js'),
    format: 'cjs',
    banner
  },
  // Runtime only (ES Modules). Used by bundlers that support ES Modules,
  // e.g. Rollup & Webpack 2
  'main-esm': {
    entry: resolvePath('src/gitee-frontend.js'),
    dest: resolvePath('dist/gitee-frontend.esm.js'),
    format: 'es',
    banner
  },
  // runtime-only production build (Browser)
  'main-prod': {
    entry: resolvePath('src/gitee-frontend.js'),
    dest: resolvePath('dist/gitee-frontend.min.js'),
    format: 'iife',
    env: 'production',
    banner
  },
  'jquery-iife': {
    entry: resolvePath('src/gitee-frontend-jquery.js'),
    dest: resolvePath('dist/jquery/gitee-frontend.js'),
    format: 'iife',
    banner
  },
  'jquery-iife-dev': {
    entry: resolvePath('src/gitee-frontend-jquery-dev.js'),
    format: 'iife',
    banner
  },
  'jquery-filtered-search-box-iife': {
    entry: resolvePath('src/components/filtered-search-box/jquery.js'),
    dest: resolvePath('dist/jquery/jquery.filtered-search-box.js'),
    format: 'iife',
    banner
  },
  'jquery-filtered-search-box-prod': {
    entry: resolvePath('src/components/filtered-search-box/jquery.js'),
    dest: resolvePath('dist/jquery/jquery.filtered-search-box.min.js'),
    format: 'iife',
    env: 'production',
    banner
  },
  'jquery-boards-iife': {
    entry: resolvePath('src/components/boards/jquery.js'),
    dest: resolvePath('dist/jquery/jquery.boards.js'),
    format: 'iife',
    banner
  },
  'jquery-boards-prod': {
    entry: resolvePath('src/components/boards/jquery.js'),
    dest: resolvePath('dist/jquery/jquery.boards.min.js'),
    format: 'iife',
    env: 'production',
    banner
  },
  'jquery-prod': {
    entry: resolvePath('src/gitee-frontend-jquery.js'),
    dest: resolvePath('dist/jquery/gitee-frontend.min.js'),
    format: 'iife',
    env: 'production',
    banner
  }
}

function genConfig(name) {
  const opts = builds[name]
  const config = {
    input: opts.entry,
    external: opts.external,
    plugins: [
      resolve(),
      commonjs(),
      vue({
        normalizer: '~vue-runtime-helpers/dist/normalize-component.js'
      }),
      name === 'jquery-iife-dev' && scss({
        output: `${path.resolve(__dirname, '..')}/demo/assets/stylesheets/gitee-frontend.css`
      }),
      babel({
        runtimeHelpers: true,
        extensions: [ '.js', '.mjs', '.vue' ]
      }),
      name === 'jquery-iife-dev' && serve({
        contentBase: [`${path.resolve(__dirname, '..')}/demo`],
      }),
      name === 'jquery-iife-dev' && livereload({
        watch: `${path.resolve(__dirname, '..')}/demo/assets`,
        verbose: false
      })
    ].concat(opts.plugins || []),
    output: {
      file: opts.dest,
      format: opts.format,
      name: opts.moduleName || 'GiteeFrontend',
      banner: opts.banner
    }
  }

  if (opts.env) {
    if (opts.env === 'production') {
      config.plugins.push(uglify({
        output: {
          comments(_node, comment) {
            if (comment.type === 'comment2') {
              return comment.value.indexOf('Released under') >= 0
            }
            return false
          }
        }
      }))
    }
    config.plugins.push(replace({
      'process.env.NODE_ENV': JSON.stringify(opts.env)
    }))
  }

  Object.defineProperty(config, '_name', {
    enumerable: false,
    value: name
  })

  return config
}

if (process.env.TARGET) {
  module.exports = genConfig(process.env.TARGET)
} else {
  exports.getBuild = genConfig
  exports.getAllBuilds = () => Object.keys(builds).map(genConfig)
}
