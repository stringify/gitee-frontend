/*!
 * Gitee-Frontend.js v0.21.1
 * (c) 2020 Gitee
 * Released under the MIT License.
 */
var ReactFilteredSearchBox = (function (react) {
  'use strict';

  function _typeof(obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

      return arr2;
    }
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArray(iter) {
    if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
  }

  function _iterableToArrayLimit(arr, i) {
    if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) {
      return;
    }

    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance");
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }

  /* eslint-disable no-console */
  var defaults = {
    filters: [],
    groups: [],
    text: {
      loading: 'loading...',
      placeholder: 'Search or filter results...',
      searchHelp: 'Press Enter or click to search',
      selectOtherFilter: 'Select other filter',
      history: 'Histroy',
      clearHistory: 'Clear history'
    },
    removeIconClass: 'icon times',
    history: {
      limit: 5,
      store: window.localStorage,
      storeKey: 'GiteeSearchHistory'
    },
    data: function data() {
      return {};
    },
    callback: function callback(data) {
      console.log(data);
    }
  };
  function Config(config) {
    $.extend(this, defaults, config);

    if (config.text) {
      $.extend(this.text, config.text);
    }
  }

  /* eslint-disable indent */
  var template = {
    container: ['<div class="filtered-search-box-input-container">', '<ul class="tokens-container">', '<li class="input-token ui search dropdown">', '<input autocomplete="off" class="search filtered-search">', '<div class="menu filter-items"></div>', '</li>', '</ul>', '</div>'].join(''),
    btnClearSearch: ['<button class="clear-search" type="button">', '<i aria-hidden="true" data-hidden="true" class="{iconClass}"></i>', '</button>'].join(''),
    itemIcon: '<i class="item-icon{class}" style="{style}"></i>',
    itemDivider: '<div class="divider"></div>',
    itemImage: '<img class="item-image ui avatar image" alt="image" src="{content}" />',
    itemDescription: '<span class="description">{content}</span>',
    inputDropdownItem: ['<div class="item{class}" data-value="{key}">', '{icon}{name}{placeholder}', '</div>'].join(''),
    filterDropdownItem: ['<div class="item{class}" data-value="{value}" data-tab="{key}">', '{icon}{image}{name}<span class="item-keywords">{keywords}</span>{description}', '</div>'].join(''),
    filterDropdownTabs: "\n    <div class=\"header tabs-wrapper\">\n  ",
    filterDropdownTab: "\n    <span class=\"tab {active}\" data-tab=\"{key}\">{name}</span>\n  ",
    filterSearchTip: "\n    <div class=\"item search-tips\" data-tab=\"visitor\">{searchTip}</div>\n  ",
    inputDropdownHeader: '<div class="header">{header}</div>',
    visualTokenValue: ['<div class="value-container{class}" style="{style}">', '<div class="value" data-value="{value}">{icon}{image}{name}</div>', '<div class="remove-token inverted" role="button">', '<i class="{iconClass}"></i>', '</div>', '</div>'].join(''),
    visualSearchTerm: ['<li class="js-visual-token filtered-search-term">', '<div class="name">{value}</div>', '</li>'].join(''),
    visualToken: ['<li class="js-visual-token filtered-search-token" data-key="{key}">', '<div class="selectable" role="button">', '<div class="name">{name}</div>', '{value}', '</div>', '</li>'].join('')
  };

  var htmlEscape = function () {
    var $el = $('<div/>');
    return function (text) {
      return $el.text(text).html();
    };
  }();
  function cleanCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
  }
  function isEmptyFilterToken($token) {
    return $token.find('.value').length < 1;
  }
  function isSearchTermToken($token) {
    return $token.hasClass('filtered-search-term');
  }
  function findListItem(list, cmp) {
    var target = null;
    list.some(function (item) {
      if (cmp(item)) {
        target = item;
        return true;
      }

      return false;
    });
    return target;
  }
  function findFilterItemByValue(filter, value) {
    var valueStr = value.toString();
    return findListItem(filter.items, function (item) {
      return item.value.toString() === valueStr;
    });
  }
  function findFilterItemByName(filter, name) {
    if (!filter.items) {
      return null;
    }

    return findListItem(filter.items, function (item) {
      return item.name === name;
    });
  }
  function compare(obj1, obj2) {
    var count = 0;
    var keys = Object.keys(obj1);
    Object.keys(obj2).forEach(function (k) {
      if (keys.indexOf(k) < 0) {
        keys.push(k);
      }
    });
    keys.forEach(function (k) {
      if (JSON.stringify(obj1[k]) === JSON.stringify(obj2[k])) {
        ++count;
      }
    });
    return count - keys.length;
  }

  function render(tmpl, data) {
    var html = tmpl;
    Object.keys(data).forEach(function (k) {
      html = html.replace('{' + k + '}', data[k]);
    });
    return html;
  }

  var itemDivider = template.itemDivider;
  function renderFilterDropdownTabs(tabs) {
    var html = $(template.filterDropdownTabs);
    tabs.forEach(function (item) {
      return html.append(render(template.filterDropdownTab, item));
    });
    return html;
  }
  function renderSearchTip(searchTip) {
    var html = render(template.filterSearchTip, {
      searchTip: searchTip
    });
    return html;
  }
  function renderFilterDropdownItem(item) {
    var html = template.filterDropdownItem;
    var keymap = {
      image: 'itemImage',
      description: 'itemDescription'
    };
    Object.keys(keymap).forEach(function (k) {
      var node = '';

      if (item[k]) {
        node = template[keymap[k]].replace('{content}', item[k]);
      }

      html = html.replace('{' + k + '}', node);
    });

    if (item.icon) {
      html = render(html, {
        icon: render(template.itemIcon, {
          "class": ' ' + item.icon,
          style: item.iconStyle || ''
        })
      });
    } else {
      html = render(html, {
        icon: ''
      });
    }

    return render(html, {
      name: htmlEscape(item.name),
      value: item.value,
      keywords: htmlEscape(item.keywords || ''),
      "class": item["class"] ? ' ' + item["class"] : '',
      key: item.key || ''
    });
  }
  function renderInputDropdownItem(item) {
    var data = {};
    data.key = item.key || 'cmd';
    data.name = item.name;

    if (item.icon) {
      data.icon = '<i class="' + item.icon + '"></i>';
    } else {
      data.icon = '';
    }

    if (item.placeholder) {
      data.placeholder = ':' + item.placeholder;
    } else {
      data.placeholder = '';
    }

    if (item["class"]) {
      data["class"] = ' ' + item["class"];
    } else {
      data["class"] = '';
    }

    return render(template.inputDropdownItem, data);
  }
  function renderItemIcon(data) {
    if (data.icon) {
      return render(template.itemIcon, {
        style: data.iconStyle || '',
        "class": ' ' + data.icon
      });
    }

    return '';
  }
  function renderInputDropdownHeader(header) {
    return render(template.inputDropdownHeader, {
      header: header
    });
  }
  function renderVisualSearchTerm(data) {
    return render(template.visualSearchTerm, {
      value: htmlEscape(data.value)
    });
  }
  var Renderer =
  /*#__PURE__*/
  function () {
    function Renderer() {
      _classCallCheck(this, Renderer);
    }

    _createClass(Renderer, [{
      key: "renderFilterTokenValueByItem",
      value: function renderFilterTokenValueByItem(item) {
        var html = template.visualTokenValue;
        html = render(html, {
          name: htmlEscape(item.name),
          value: item.value,
          iconClass: this.config.removeIconClass
        });
        html = render(html, {
          icon: renderItemIcon(item)
        });

        if (item.image) {
          html = render(html, {
            image: render(template.itemImage, {
              content: item.image
            })
          });
        } else {
          html = render(html, {
            image: ''
          });
        }

        if (item.color) {
          return render(html, {
            "class": ' has-color',
            style: 'background-color: ' + item.color
          });
        }

        return render(html, {
          "class": '',
          style: ''
        });
      }
    }, {
      key: "renderFilterTokenValue",
      value: function renderFilterTokenValue(filter, value) {
        var i;
        var item;
        var items = filter.items;
        var valueStr = value.toString();

        if (filter.type === 'daterange') {
          return this.renderFilterTokenValueByItem({
            name: value,
            value: value
          });
        }

        if (_typeof(filter.none) === 'object') {
          items = [filter.none].concat(items);
        }

        for (i = 0; i < items.length; ++i) {
          item = items[i];

          if (item.value.toString() === valueStr) {
            return this.renderFilterTokenValueByItem(item);
          }
        }

        return '';
      }
    }, {
      key: "renderVisualToken",
      value: function renderVisualToken(filter, value) {
        var _this = this;

        var values;
        var valuesHtml = '';
        var html = template.visualToken;

        if (filter.multiple) {
          values = value;
        } else {
          values = [value];
        }

        html = render(template.visualToken, filter);

        if (typeof value !== 'undefined') {
          values.forEach(function (val) {
            valuesHtml += _this.renderFilterTokenValue(filter, val);
          });
        }

        html = html.replace('{value}', valuesHtml);
        return html;
      }
    }, {
      key: "renderBody",
      value: function renderBody() {
        return [template.container, render(template.btnClearSearch, {
          iconClass: this.config.removeIconClass
        })].join('');
      }
    }]);

    return Renderer;
  }();

  /* eslint-disable no-param-reassign */

  function onIntent(e) {
    var i;
    var item;
    var that = e.data["this"];
    var $target = $(e.target);
    var classes = ['.filtered-search-box'];
    that.logger.log('onIntent()'); // 如果点击的是组件内的元素，则保持激活状态

    for (i = 0; i < classes.length; ++i) {
      if ($target.parents(classes[i]).length > 0) {
        that.lockShow();
        that.focus();
        return;
      }
    }

    if (!that.target) {
      return;
    }

    that.active = false; // 如果输入框后面没有筛选器，就不移动输入框了

    if (that.$inputDropdown.next('.js-visual-token').length < 1) {
      return;
    }

    item = findFilterItemByName(that.target, that.targetValue);

    if (item) {
      that.selectFilterValue(item.value);
    }

    that.$input.val('');
    that.clearTargetFilter();
    that.resetInputPosition();
  } // 初始化筛选器的事件处理


  function initTokenEventHandler(that) {
    that.$el.on('click', '.js-visual-token', function (e) {
      var $value;
      var $target = $(e.target);
      var $token = $target.parents('.js-visual-token');
      e.stopPropagation(); // 如果点击的是删除按钮

      if ($target.hasClass('remove-token') || $target.parent().hasClass('remove-token')) {
        $value = $target.parents('.value-container').find('.value');
        that.removeFilterValue($token, $value.data('value')); // 如果当前输入框已经获得焦点，则让它保持焦点

        if (that.hasFocus()) {
          that.focus();
          that.deferFocusTime();
        }

        return;
      }

      that.editToken($token);
    });
  } // 初始化输入框内的输入事件捕获，用于处理用户手动输入的筛选条件


  function initInputTokenCatch(that) {
    var keyHandlers = {
      'Backspace': function Backspace(value) {
        var $token; // 只在输入框内容为空时处理条件删除操作

        if (value) {
          return;
        }

        if (that.target) {
          $token = that.$target; // 如果该筛选标记元素没有值，则切换编辑筛选器名称

          if (isEmptyFilterToken($token)) {
            value = that.target.name;
            that.clearTargetFilter();
            that.removeFilterToken($token);
          } else {
            that.popCurrentFilterValue();
            that.buildInputDropdown();
          }

          return;
        } // 取上一个筛选器，然后删除它的最后一个选项


        $token = that.$inputDropdown.prev('.js-visual-token');

        if ($token.length > 0) {
          if (isSearchTermToken($token)) {
            that.editTerm();
            return;
          }

          that.popFilterValue($token);

          if (isEmptyFilterToken($token)) {
            $token.remove();
          }

          that.buildInputDropdown();
        }
      },
      'Enter': function Enter() {
        if (!that.target) {
          that.submit();
          that.buildInputDropdown();
          setTimeout(function () {
            that.$input.focus();
          }, 10);
        }
      },
      ':': function _(value) {
        var filter;

        if (that.target) {
          that.$input.val(value);
          return;
        }

        filter = that.getFilterByName(value);

        if (filter && that.newFilter(filter.key)) {
          that.$input.val('');
          return;
        }

        that.$input.val(value);
      },
      // 筛选选项分割符，针对可多选的筛选器
      ',': function _(value) {
        var item;

        if (!that.target) {
          return;
        }

        item = findFilterItemByName(that.target, value);

        if (item && that.selectFilterValue(item.value)) {
          that.$input.val('');
          that.input = '';
          that.buildFilterDropdown(that.target);
        } else {
          that.$input.val(value);
        }
      },
      // 筛选结束符
      ';': function _(value) {
        var item;

        if (!that.target) {
          that.$input.val(value);
          return;
        }

        item = findFilterItemByName(that.target, value);

        if (item && that.selectFilterValue(item.value)) {
          that.$input.val('');
          that.input = '';
          that.clearTargetFilter();
        } else {
          that.$input.val(value);
        }
      }
    };
    that.$input.on({
      focus: function focus() {
        that.$el.addClass('focus');
      },
      blur: function blur() {
        that.$el.removeClass('focus');
        /**
        that.active = false
        that.deferFocusTime() */
      },
      input: function input() {
        that.input = that.$input.val();
        that.updateState();

        if (that.target && that.target.searchOption) {
          var key = that.target.key;

          if (that.target.timeOutId) {
            clearTimeout(that.target.timeOutId);
          }

          that.target.timeOutId = setTimeout(function () {
            if (that.input.length) {
              if (!that.target || !that.target.searchOption) return;
              that.target.searchOption.fetcher(that.input, function (data) {
                if (key === that.target.key) {
                  that.target.items = that.target.items.filter(function (item) {
                    return item.key !== that.target.searchOption.key;
                  });
                  var items = that.target.searchOption.converter(data);
                  that.target.items = that.target.items.concat(items);
                  that.buildRemoteSearchDropdownItems(that.target.searchOption.key, items);
                }

                that.setTabSelected();
              });
            } else {
              that.buildRemoteSearchDropdownItems(that.target.searchOption.key, []);

              if (that.$inputDropdownMenu.find('.item').not('.filtered').length) {
                that.buildSearchTip(that.target.searchOption.searchTip);
              }

              that.$el.find(".tab[data-tab=".concat(that.target.tabs[0].key, "]")).trigger('click');
            }
          }, 400);
        }
      }
    });
    that.$el.on('click', function () {
      that.focus();
    });
    that.$input.on('keydown', function (e) {
      var val = that.$input.val();

      if (!keyHandlers[e.key]) {
        return;
      } // 延迟一会，避免在给输入框赋值后又被后面的 inout 事件重置


      setTimeout(function () {
        keyHandlers[e.key](val);
      }, 10);
    });
  }

  function initTabEventHandler(target) {
    target.$el.on('click', '.tab', function (e) {
      target.$inputDropdownMenu.find('.tab').removeClass('active');
      $(e.target).addClass('active');
      target.$inputDropdownMenu.find('.item').addClass('hide');
      target.$inputDropdownMenu.find(".item[data-tab=".concat($(e.target).data('tab'), "]")).removeClass('hide');
    });
  } // 初始化“清除”按钮


  function initClearButton(that) {
    that.$btnClearSearch.on('click', function () {
      that.clear();
      that.commit();
    });
  }

  function bindIntent(target) {
    $(document).on('mousedown', {
      "this": target
    }, onIntent);
  }
  function unbindIntent() {
    $(document).off('mousedown', onIntent);
  } // 阻止已冒泡到 document 的 click 事件的处理

  function stopIntent(that) {
    unbindIntent();
    setTimeout(function () {
      bindIntent(that);
    }, 100);
  }
  function bindEventHandlers(target) {
    initTokenEventHandler(target);
    initInputTokenCatch(target);
    initClearButton(target);
    initTabEventHandler(target);
  }
  function unbindEventHandlers() {
    unbindIntent();
  }

  var Builder =
  /*#__PURE__*/
  function (_Renderer) {
    _inherits(Builder, _Renderer);

    function Builder() {
      _classCallCheck(this, Builder);

      return _possibleConstructorReturn(this, _getPrototypeOf(Builder).apply(this, arguments));
    }

    _createClass(Builder, [{
      key: "buildFilterDropdownItems",

      /**
       * 构建筛选器的下拉菜单选项
       * @param {Object} filter 筛选器对象
       */
      value: function buildFilterDropdownItems(filter) {
        var that = this;
        var item = {};
        that.$inputDropdownMenu.empty(); // 如果条件支持多选，则添加一条选项，让用户点击退出此条件的选择

        if (filter.multiple) {
          that.$inputDropdownMenu.append(renderInputDropdownItem({
            key: '<cmd:back>',
            icon: 'icon angle double left',
            "class": 'item-fixed',
            name: that.text.selectOtherFilter
          }), itemDivider);
        } // 如果有设置“无“选项


        if (_typeof(filter.none) === 'object') {
          Object.keys(filter.none).forEach(function (k) {
            item[k] = filter.none[k];
          });

          if (item["class"]) {
            item["class"] = "".concat(item["class"], " item-fixed");
          } else {
            item["class"] = 'item-fixed';
          }

          that.$inputDropdownMenu.append(renderFilterDropdownItem(item), itemDivider);
        }

        if (filter.tabs && filter.tabs.length) {
          that.$inputDropdownMenu.append(renderFilterDropdownTabs(filter.tabs));
        }

        this.getFilterAvailableItems(filter).forEach(function (filterItem) {
          that.$inputDropdownMenu.append(renderFilterDropdownItem(filterItem));
        });

        if (filter.searchOption) {
          that.$inputDropdownMenu.append(renderSearchTip(filter.searchOption.searchTip));
        }

        if (filter.tabs && filter.tabs.length) {
          that.$el.find(".tab[data-tab=".concat(filter.tabs[0].key, "]")).addClass('active');
          that.$inputDropdownMenu.find('.item').addClass('hide');
          that.$inputDropdownMenu.find(".item[data-tab=".concat(filter.tabs[0].key, "]")).removeClass('hide');
        }

        that.$inputDropdown.dropdown('refresh').dropdown('search'); // .dropdown('search') 防止远程结果为空时下拉框显示有问题
      }
    }, {
      key: "buildRemoteSearchDropdownItems",
      value: function buildRemoteSearchDropdownItems(key, items) {
        var _this = this;

        this.$inputDropdownMenu.find(".item[data-tab=".concat(key, "]")).remove();
        items.forEach(function (filterItem) {
          _this.$inputDropdownMenu.append(renderFilterDropdownItem(filterItem));
        });
      }
    }, {
      key: "buildSearchTip",
      value: function buildSearchTip(searchTip) {
        this.$inputDropdownMenu.append(renderSearchTip(searchTip));
      }
    }, {
      key: "buildFilterDropdownRemoteTabItems",
      value: function buildFilterDropdownRemoteTabItems(filter) {
        var that = this;
        that.$inputDropdownMenu.empty().append(renderInputDropdownItem({
          key: '<cmd:wait>',
          icon: 'ui active inline mini loader',
          "class": 'item-fixed',
          name: that.text.loading,
          type: filter.type
        }));
        that.loadFilterRemoteItems(filter, function () {
          if (that.target && that.target.key === filter.key) {
            that.buildFilterDropdownItems(filter);
          }
        });
      }
    }, {
      key: "buildFilterDropdownRemoteItems",
      value: function buildFilterDropdownRemoteItems(filter) {
        var that = this;
        that.$inputDropdownMenu.empty().append(renderInputDropdownItem({
          key: '<cmd:wait>',
          icon: 'ui active inline mini loader',
          "class": 'item-fixed',
          name: that.text.loading,
          type: filter.type
        }));
        that.loadFilterRemoteItems(filter, function () {
          if (that.target && that.target.key === filter.key) {
            that.buildFilterDropdownItems(filter);
          }
        });
      }
      /**
       * 构建时间范围选择器
       * @param {Object} filter 筛选器对象
       */

    }, {
      key: "buildDateRangePicker",
      value: function buildDateRangePicker(filter) {
        var picker;
        var that = this;
        var value = this.targetValue;
        var values;
        that.$inputDropdown.dropdown({
          on: 'custom',
          onShow: function onShow() {
            return false;
          }
        }).dropdown('hide');
        that.removeDateRangePicker();
        that.$input.daterangepicker(filter.config);
        picker = that.$input.data('daterangepicker');

        if (value) {
          values = value.split(picker.locale.separator);
          picker.setStartDate(values[0]);
          picker.setEndDate(values[1]);
        }

        that.$input.on({
          'apply.daterangepicker': function applyDaterangepicker() {
            var start = picker.startDate.format(picker.locale.format);
            var end = picker.endDate.format(picker.locale.format);
            that.selectFilterValue(start + picker.locale.separator + end);
            that.clearTargetFilter();
            that.$input.val('');
            picker.remove();
          },
          'cancel.daterangepicker': function cancelDaterangepicker() {
            that.selectFilterValue(value);
            that.clearTargetFilter();
            that.$input.val('');
            picker.remove();
          }
        });
        picker.show();
      }
      /**
       * 构建筛选器的下拉框，列出当前筛选器的可选选项
       * @param {Object} filter 筛选器对象
       */

    }, {
      key: "buildFilterDropdown",
      value: function buildFilterDropdown(filter) {
        var that = this;
        that.updatePlaceholder();

        if (filter.type === 'daterange') {
          this.buildDateRangePicker(filter);
          return;
        }

        if (filter.tabs && filter.tabs.length) {
          this.$tokens.find('.menu.transition.visible').addClass('with-tabs');
          that.buildFilterDropdownRemoteTabItems(filter);
        } else {
          this.$tokens.find('.menu.transition.visible').removeClass('with-tabs');

          if (_typeof(filter.remote) === 'object') {
            that.buildFilterDropdownRemoteItems(filter);
          } else {
            that.buildFilterDropdownItems(filter);
          }
        }

        that.$inputDropdown.removeClass('input-filter-token').addClass('input-filter-value-token').dropdown({
          forceSelection: false,
          fullTextSearch: true,
          selectOnKeydown: false,
          onHide: function onHide() {
            that.logger.log('buildFilterDropdown() hide?', !that.hasFocus()); // 根据当前状态确定是否阻止隐藏下拉框

            return that.onHide();
          },
          action: function action(_text, value) {
            var item;
            that.lockShow();

            if (!value) {
              return true;
            }

            if (that.execCommand(value)) {
              return true;
            }

            that.input = '';
            that.targetValue = '';

            if (_typeof(filter.none) === 'object' && filter.none.value.toString() === value) {
              that.clearFilterValue();
              that.selectFilterValue(value);
              that.clearTargetFilter();
              return false;
            }

            item = that.getCurrentFilterItem(value);

            if (item) {
              that.selectFilterValue(value);
            } // 如果是多选，则重置下拉框


            if (filter.multiple) {
              // 没有可选选项就退出编辑筛选器
              if (!that.visibleFilter(filter)) {
                that.clearTargetFilter();
              } else {
                that.buildFilterDropdown(filter);
                that.$input.val('');
                that.$input.focus();

                if (filter.tabs) {
                  that.setTabSelected();
                }
              }
            } else {
              that.clearTargetFilter();
            }

            stopIntent(that);
            return false;
          },
          onChange: function onChange(value) {
            var item;
            that.lockShow();

            if (value && value.indexOf('<cmd:') === 0) {
              return;
            }

            item = that.getCurrentFilterItem(value);

            if (item) {
              that.$input.val(item.name);
            }
          }
        }).dropdown('clear');
        this.logger.log('[' + filter.key + ']', 'buildFilterDropdown()');
      }
      /**
       * 构建已分组的下拉菜单选项
       * @param {Array} 分组参数列表
       * @returns {Array} 已被分组的筛选器的 key 列表
       */

    }, {
      key: "buildGroupedDropdownItems",
      value: function buildGroupedDropdownItems(groups) {
        var that = this;
        var groupedFilters = [];
        groups.forEach(function (group) {
          var html = '';
          that.filters.forEach(function (filter) {
            if (group.keys.indexOf(filter.key) < 0) {
              return;
            }

            groupedFilters.push(filter.key);

            if (!that.visibleFilter(filter)) {
              return;
            }

            html += renderInputDropdownItem(filter);
          });

          if (html) {
            html = renderInputDropdownHeader(group.name) + html;
          }

          that.$inputDropdownMenu.append(html);
        });
        return groupedFilters;
      }
    }, {
      key: "buildHistoryItems",
      value: function buildHistoryItems() {
        if (this.config.history && this.history.load().length > 0) {
          this.$inputDropdownMenu.append(renderInputDropdownHeader(this.text.history), this.history.render(), renderInputDropdownItem({
            key: '<cmd:clear-history>',
            name: this.text.clearHistory
          }), itemDivider);
        }
      }
    }, {
      key: "clearSearchItem",
      value: function clearSearchItem() {
        this.$input.off('input.dropdown');
      } // 构建“搜索”选项

    }, {
      key: "buildSearchItem",
      value: function buildSearchItem() {
        var that = this;
        var $cmd = $(renderInputDropdownItem({
          key: '<cmd:search>',
          icon: 'icon search',
          "class": 'item-fixed',
          name: that.text.searchHelp
        }));
        that.$inputDropdownMenu.append($cmd); // 在输入内容时，更新“搜索”选项的值，让下拉框能将它作为第一个匹配的选项，并默认选中它

        that.$input.on('input.dropdown', function () {
          // 将输入框当前值放在前面，确保下拉框能够搜索匹配到
          $cmd.attr('data-value', that.$input.val() + ';<cmd:search>');
          that.$inputDropdown.dropdown('refresh');
        });
      }
    }, {
      key: "buildInputDropdown",
      value: function buildInputDropdown() {
        var html = '';
        var that = this;
        var groupedFilters = [];
        this.clearSearchItem();

        if (that.target) {
          that.buildFilterDropdown(that.target);
          return;
        }

        that.updatePlaceholder();
        that.logger.log('buildInputDropdown()');
        that.$inputDropdownMenu.empty();
        that.buildSearchItem();
        that.buildHistoryItems();

        if (that.groups.length > 0) {
          groupedFilters = that.buildGroupedDropdownItems(that.groups);
        }

        that.filters.forEach(function (filter) {
          if (groupedFilters.indexOf(filter.key) >= 0) {
            return;
          }

          if (!that.visibleFilter(filter)) {
            return;
          }

          html += renderInputDropdownItem(filter);
        });

        if (html && groupedFilters.length > 0) {
          html = itemDivider + html;
        }

        that.$inputDropdownMenu.append(html);
        that.$inputDropdown.addClass('input-filter-token').removeClass('input-filter-value-token').dropdown({
          forceSelection: false,
          selectOnKeydown: false,
          onHide: function onHide() {
            if (that.input) {
              that.applyInput();
            }

            if (!that.hasFocus()) {
              that.resetInputPosition();
            }

            that.logger.log('buildInputDropdown() hide?', !that.hasFocus());
            return that.onHide();
          },
          action: function action(_text, value) {
            that.logger.log('action():', value);
            that.lockShow();

            if (value) {
              if (that.newFilter(value)) {
                return;
              }
            }

            if (that.input) {
              that.applyInput();
              that.buildInputDropdown();
              that.execCommand(value);
            } else if (that.execCommand(value)) {
              // 清空下拉框的值，以取消选项的“已选中”的效果
              that.$inputDropdown.dropdown('clear');
              that.logger.log('execCommand');
              that.applyInput();
            }
          }
        }).dropdown('clear');
      } // 构建已选中的筛选器标记

    }, {
      key: "buildFilterTokens",
      value: function buildFilterTokens() {
        var that = this;
        that.loadingFilters = 0;
        that.filters.forEach(function (filter) {
          var html;
          var value = that.data[filter.key];
          that.logger.log('buildFilterTokens():', filter.key);

          if (typeof value === 'undefined') {
            return;
          }

          if (filter.multiple) {
            if (value.length < 1) {
              return;
            }
          } else if (value === '') {
            return;
          }

          if (that.checkFilterItemsLoaded(filter)) {
            html = that.renderVisualToken(filter, value);
            $(html).insertBefore(that.$inputDropdown);
            return;
          }

          that.setDisabled(true);
          that.loadingFilters += 1;
          that.loadFilterRemoteItems(filter, function () {
            var selector = '.js-visual-token[data-key="' + filter.key + '"]';
            that.$el.find(selector).remove();
            html = that.renderVisualToken(filter, that.data[filter.key]);
            $(html).insertBefore(that.$inputDropdown);
            that.loadingFilters -= 1;

            if (that.loadingFilters < 1) {
              that.setDisabled(false);
              that.buildSearchTerm();
            }
          });
        });
      } // 更新搜索条件的内容及位置

    }, {
      key: "buildSearchTerm",
      value: function buildSearchTerm() {
        if (this.$search && this.$search.length > 0) {
          if (this.data.search) {
            this.$search.find('.name').text(this.data.search);
          } else {
            this.$search.remove();
            this.$search = null;
            return;
          }
        } else if (this.data.search) {
          this.$search = $(renderVisualSearchTerm({
            value: this.data.search
          }));
        } else {
          return;
        } // 移动元素时会触发下拉框的 hide 行为，而 onHide() 回调里有调用 applyInput() 方法，
        // 等于递归调用了一次，为避免重复记录搜索词，在这里加个延迟


        setTimeout(function () {
          // 顺带更新条件和输入框的位置
          this.deferFocusTime();
          this.$tokens.append(this.$search, this.$inputDropdown);
        }.bind(this));
      } // 构建组件基础内容和功能

    }, {
      key: "build",
      value: function build() {
        this.$el.html(this.renderBody());
        this.$el.addClass('filtered-search-box');
        this.$tokens = this.$el.find('.tokens-container');
        this.$input = this.$el.find('.filtered-search');
        this.$inputDropdown = this.$input.parent();
        this.$inputDropdownMenu = this.$inputDropdown.find('.filter-items');
        this.$btnClearSearch = this.$el.find('.clear-search');
        this.buildInputDropdown();
        this.buildFilterTokens();
        this.buildSearchTerm();
        bindIntent(this);
        this.updateClearButton();
      }
    }]);

    return Builder;
  }(Renderer);

  function renderHistroyItem(item) {
    return renderInputDropdownItem({
      name: item.name,
      "class": 'history-item',
      key: '<cmd:apply:' + escape(JSON.stringify(item.value)) + '>'
    });
  }

  var History =
  /*#__PURE__*/
  function () {
    function History(config) {
      _classCallCheck(this, History);

      if (_typeof(config) === 'object') {
        $.extend(this, defaults.history, config);
      } else {
        $.extend(this, defaults.history);
      }
    }

    _createClass(History, [{
      key: "load",
      value: function load() {
        try {
          var data = JSON.parse(this.store.getItem(this.storeKey));

          if (data instanceof Array) {
            return data;
          }
        } catch (e) {
          return [];
        }

        return [];
      }
    }, {
      key: "generateItem",
      value: function generateItem(filters, data) {
        var name = []; // 生成条件组名称

        filters.forEach(function (filter) {
          var value = data[filter.key];

          if (!value) {
            return;
          }

          if (filter.none && value.toString() === filter.none.value.toString()) {
            name.push(filter.name + ': ' + filter.none.name);
            return;
          }

          if (filter.multiple && value instanceof Array) {
            var values = [];
            value.forEach(function (v) {
              var item = findFilterItemByValue(filter, v);

              if (item) {
                values.push(item.name);
              }
            });

            if (values.length > 0) {
              name.push(filter.name + ': ' + values.join(', '));
            }

            return;
          }

          if (filter.type === 'daterange') {
            name.push(filter.name + ': ' + value);
            return;
          }

          var item = findFilterItemByValue(filter, value);

          if (item) {
            name.push(filter.name + ': ' + item.name);
          }
        });

        if (data.search) {
          name.push(data.search);
        }

        if (name.length < 1) {
          return false;
        }

        name = name.join('; ');
        return {
          name: name,
          value: data
        };
      }
      /**
       * 添加历史记录
       * @param {Array} filters 筛选器列表，用于为条件组生成名称
       */

    }, {
      key: "add",
      value: function add(filters, data) {
        var history = this.load();
        var item = this.generateItem(filters, data); // 删除同名的历史记录

        for (var i = 0; i < history.length; ++i) {
          if (history[i].name === item.name) {
            history.splice(i, 1);
            break;
          }
        }

        history.splice(0, 0, item);
        history = history.splice(0, this.limit);
        this.store.setItem(this.storeKey, JSON.stringify(history));
        return true;
      }
    }, {
      key: "clear",
      value: function clear() {
        this.store.removeItem(this.storeKey);
      }
    }, {
      key: "render",
      value: function render() {
        var html = [];
        var history = this.load();
        history.forEach(function (item) {
          html.push(renderHistroyItem(item));
        });
        return html.join('');
      }
    }]);

    return History;
  }();

  var handlers = {
    search: function search() {
      this.submit();
    },
    back: function back() {
      var item = null;

      if (this.targetValue) {
        item = findFilterItemByName(this.target, this.targetValue);
      }

      this.input = '';
      this.targetValue = '';

      if (item) {
        this.selectFilterValue(item.value);
        this.clearTargetFilter(); // 移动输入框到筛选器列表的末尾

        this.$tokens.append(this.$inputDropdown);
      } else {
        this.clearTargetFilter();
      }

      this.$input.focus();
    },
    apply: function apply(paramsStr) {
      var params = JSON.parse(unescape(paramsStr));
      this.active = true;
      this.setData(params);
      this.commit();
    },
    'clear-history': function clearHistory() {
      this.history.clear();
      this.buildInputDropdown();
    }
  };
  function exec(_this, cmd) {
    var i = cmd.indexOf('<cmd:');

    if (i < 0) {
      return false;
    }

    var _cmd$slice$split = cmd.slice(i + 5, -1).split(':'),
        _cmd$slice$split2 = _slicedToArray(_cmd$slice$split, 2),
        name = _cmd$slice$split2[0],
        params = _cmd$slice$split2[1];

    if (handlers[name]) {
      handlers[name].call(_this, params);
      return true;
    }

    return false;
  }

  var FilteredSearchBox =
  /*#__PURE__*/
  function (_Builder) {
    _inherits(FilteredSearchBox, _Builder);

    function FilteredSearchBox($el, options) {
      var _this;

      _classCallCheck(this, FilteredSearchBox);

      var config = new Config(options);
      _this = _possibleConstructorReturn(this, _getPrototypeOf(FilteredSearchBox).call(this));
      _this.config = config;
      _this.text = config.text;
      _this.data = config.data();
      _this.groups = config.groups;
      _this.filters = config.filters;
      _this.callback = config.callback;
      _this.input = '';
      _this.store = {};
      _this.active = false;
      _this.target = null;
      _this.targetValue = null;
      _this.history = new History(config.history);
      _this.$el = $el;
      _this.$search = null;
      _this.$tokens = null;
      _this.$target = null;
      _this.$targetValues = null;
      _this.logger = {
        log: function log() {}
      };

      if (options.debug) {
        _this.logger = console;
      }

      if (typeof _this.data.search !== 'string') {
        _this.data.search = '';
      }

      _this.build();

      bindEventHandlers(_assertThisInitialized(_this));
      return _this;
    } // 清空选择器的选项


    _createClass(FilteredSearchBox, [{
      key: "clearFilterValue",
      value: function clearFilterValue() {
        var filter = this.target;
        var $token = this.$target;
        var values = this.data[filter.key];
        var $values = this.$targetValues.find('.value-container');

        if (filter.multiple && values) {
          $token.find('.value').each(function () {
            var i;
            var value = $(this).data('value');

            if (typeof value === 'undefined') {
              return;
            }

            value = value.toString();

            for (i = 0; i < values.length; ++i) {
              if (values[i].toString() === value) {
                values.splice(i, 1);
                break;
              }
            }
          });

          if (values.length < 1) {
            delete this.data[filter.key];
          }
        } else {
          delete this.data[filter.key];
        }

        $values.remove();
        this.commit();
        this.buildFilterDropdown(filter);
      }
      /**
       * 清除筛选器依赖表里的筛选器
       * 一个筛选器被其它筛选器依赖后，当这个筛选器的值被改变时，其它依赖筛选器也需要清除
       * @param {Object} filterKey 筛选器标识
       * @returns {Number} 清除的筛选器的数量
       */

    }, {
      key: "clearRequiredFilters",
      value: function clearRequiredFilters(filterKey) {
        var that = this;
        var count = 0;
        that.filters.forEach(function (f) {
          if (f.requires instanceof Array) {
            if (f.requires.indexOf(filterKey) >= 0) {
              that.removeFilter(f.key);
              count += 1;
            }
          }
        });
        return count;
      }
      /**
       * 为筛选选器选择一个选项
       * @param {String} value 选项值
       * @returns {Boolean} 是否成功选中
       */

    }, {
      key: "selectFilterValue",
      value: function selectFilterValue(value) {
        var html;
        var filter = this.target;
        var $values = this.$targetValues;
        var values = this.data[filter.key];

        if (!$values) {
          return false;
        }

        if (filter.multiple) {
          if (values instanceof Array && values.indexOf(value.toString()) >= 0) {
            return false;
          }
        } else if (values === value.toString()) {
          return false;
        }

        html = this.renderFilterTokenValue(filter, value);

        if (html) {
          $values.append(html);

          if (filter.multiple) {
            if (this.data[filter.key] instanceof Array) {
              this.data[filter.key].push(value);
            } else {
              this.data[filter.key] = [value];
            }
          } else {
            this.data[filter.key] = value;
          }

          this.clearRequiredFilters(filter.key);
          this.commit();
          return true;
        }

        return false;
      }
      /**
       * 移除当前筛选器中的一个选项标记
       * @param {jQuery} $token 筛选器标记
       * @param {String} value 筛选选项值
       * @returns {Boolean} 是否移除成功
       */

    }, {
      key: "removeFilterValue",
      value: function removeFilterValue($token, value) {
        var values;
        var filter;
        filter = this.getFilterByKey($token.data('key'));

        if (!filter) {
          return false;
        }

        $token.find('.value[data-value="' + value + '"]').parent().remove();
        values = this.data[filter.key];

        if (filter.multiple) {
          value = value.toString();
          values.some(function (v, i) {
            if (v.toString() === value) {
              values.splice(i, 1);
              return true;
            }

            return false;
          });
        } else {
          delete this.data[filter.key];
        }

        if (isEmptyFilterToken($token)) {
          // 如果不是当前正编辑的筛选条件，则移除筛选器并取消编辑对象
          if (!this.target || this.target.key !== filter.key) {
            this.removeFilterToken($token);
            this.clearTargetFilter();
          }
        }

        this.clearRequiredFilters(filter.key);
        this.buildInputDropdown();
        this.commit();
        return true;
      }
    }, {
      key: "doRemoveFilter",
      value: function doRemoveFilter(filterKey) {
        var $token;
        delete this.data[filterKey];
        $token = this.$el.find('.js-visual-token[data-key="' + filterKey + '"]');
        $token.remove();
      }
    }, {
      key: "removeFilter",
      value: function removeFilter(filterKey) {
        this.doRemoveFilter(filterKey);
        this.clearRequiredFilters(filterKey);
        this.buildInputDropdown();
        this.commit();
      }
      /**
       * 移除一个筛选器标记元素
       * @param {jQuery} $token 筛选器标记元素
       */

    }, {
      key: "removeFilterToken",
      value: function removeFilterToken($token) {
        var key = $token.data('key');
        var values = this.data[key];
        var filter = this.getFilterByKey(key);

        if (!filter) {
          return false;
        }

        if (filter.type === 'daterange') {
          delete this.data[key];
        } else if (filter.multiple && values) {
          $token.find('.value').each(function () {
            var i;
            var value = $(this).data('value');

            if (typeof value === 'undefined') {
              return;
            }

            value = value.toString();

            for (i = 0; i < values.length; ++i) {
              if (values[i].toString() === value) {
                values.splice(i, 1);
                break;
              }
            }
          });

          if (values.length < 1) {
            delete this.data[key];
          }
        } else {
          delete this.data[key];
        }

        $token.remove();
        this.clearRequiredFilters(key);
        this.buildInputDropdown();
        this.commit();
        return true;
      }
    }, {
      key: "newFilter",
      value: function newFilter(filterKey) {
        var $token;
        var filter = null;
        this.logger.log('newFilter()', filterKey);
        filter = this.getFilterByKey(filterKey);

        if (!filter || !this.visibleFilter(filter)) {
          return false;
        }

        $token = this.$el.find('[data-key="' + filterKey + '"]');

        if ($token.length < 1) {
          $token = $(this.renderVisualToken(filter));
          $token.insertBefore(this.$inputDropdown);
        }

        this.selectFilterToken($token);
        this.updateState();
        return true;
      } // 编辑搜索条件内容

    }, {
      key: "editTerm",
      value: function editTerm() {
        if (this.$search) {
          this.$search.remove();
        }

        this.$search = null;
        this.buildInputDropdown();
        this.input = this.data.search;
        this.data.search = '';
        this.$input.val(this.input);
        this.$input.focus();
      }
      /**
       * 编辑筛选器
       * @param {jQuery} $token 目标筛选器的标记元素
       * @returns {Boolean} 是否成功切换编辑状态
       */

    }, {
      key: "editFilter",
      value: function editFilter($token) {
        var filter = this.getFilterByKey($token.data('key'));

        if (this.target && this.target.key === filter.key) {
          this.$input.focus();
          return true;
        } // 如果当前有正编辑的筛选器，则保存它


        if (this.targetValue) {
          var item = findFilterItemByName(this.target, this.targetValue);

          if (item) {
            this.selectFilterValue(item.value);
          }
        }

        this.logger.log('editFilter()');

        if (this.selectFilterToken($token)) {
          this.editCurrentFilterValue();
          return true;
        }

        return false;
      }
    }, {
      key: "editToken",
      value: function editToken($token) {
        if (isSearchTermToken($token)) {
          this.editTerm();
        } else {
          this.editFilter($token);
        }
      }
      /**
       * 获取筛选器标记元素
       * @param {Object} filter 筛选器对象
       * @param {jQuery} $anchor 作为锚点的标记元素（可选），如果指定该参数，则会以它为起点，
       * 在它前面的元素中查找匹配的筛选器标记元素。
       */

    }, {
      key: "getFilterToken",
      value: function getFilterToken(filter, $anchor) {
        var $token;
        var selector = '.js-visual-token[data-key="' + filter.key + '"]';

        if ($anchor) {
          $token = $anchor.prev(selector);
        } else {
          $token = this.$el.find(selector);
        }

        if ($token.length < 1) {
          return null;
        }

        return $token.last();
      }
      /**
       * 取出指定筛选器的一个选项
       * @param {jQuery} $token 筛选器标记元素
       * @returns {String} 取出的选项内容，如果没有则返回 null
       */

    }, {
      key: "popFilterValue",
      value: function popFilterValue($token) {
        var value;
        var item;
        var filter = this.getFilterByKey($token.data('key'));

        if (!filter) {
          return null;
        }

        if (filter.type === 'daterange') {
          $token.find('.value-container').remove();
          value = this.data[filter.key];
          delete this.data[filter.key];
        } else {
          if (filter.multiple) {
            value = this.data[filter.key].pop();
            $token.find('.value[data-value="' + value + '"]').parent().remove();
          } else {
            $token.find('.value-container').remove();
            value = this.data[filter.key];
            delete this.data[filter.key];
          }

          item = findFilterItemByValue(filter, value);

          if (item) {
            value = item.name;
          } else if (_typeof(filter.none) === 'object') {
            if (filter.none.value.toString() === value.toString()) {
              value = filter.none.name;
            }
          }
        }

        this.logger.log('[' + filter.key + '] pop value:', value);
        return value;
      }
      /**
       * 取出当前选中筛选器的一个选项
       * @returns {String} 取出的选项内容，如果没有则返回 null
       */

    }, {
      key: "popCurrentFilterValue",
      value: function popCurrentFilterValue() {
        return this.popFilterValue(this.$target);
      }
      /**
       * 取出当前选中筛选器的一个选项进行编辑
       * @returns {String} 取出的选项内容，如果没有则返回 null
       */

    }, {
      key: "editCurrentFilterValue",
      value: function editCurrentFilterValue() {
        var value = this.popCurrentFilterValue();

        if (!value) {
          return null;
        }

        this.targetValue = value; // 将输入框移动到目标筛选条件后面

        this.$inputDropdown.insertAfter(this.$target);
        this.buildInputDropdown();
        this.$input.val(value).focus();
        return value;
      }
      /**
       * 选中一个筛选器，作为当前编辑对象
       * @param {jQuery} $token 筛选器标记元素
       * @return {Boolean} 是否选中成功
       */

    }, {
      key: "selectFilterToken",
      value: function selectFilterToken($token) {
        var filter = this.getFilterByKey($token.data('key'));

        if (!filter) {
          return false;
        }

        if (this.target) {
          if (this.target.key === filter.key) {
            return true;
          }
        }

        this.input = '';
        this.target = filter;
        this.$target = $token;
        this.$targetValues = $token.find('.selectable'); // 将输入框定位到筛选器标记元素的后面

        this.$inputDropdown.insertAfter($token);
        this.buildFilterDropdown(this.target);
        this.$input.focus();
        return true;
      }
    }, {
      key: "clearTargetFilter",
      value: function clearTargetFilter() {
        var that = this;
        var picker;

        if (that.target) {
          if (isEmptyFilterToken(that.$target)) {
            that.$target.remove();
          }

          if (that.target.type === 'daterange') {
            picker = that.$input.data('daterangepicker');

            if (picker) {
              picker.remove();
            }
          }

          that.applyInput();
        }

        that.target = null;
        that.targetValue = '';
        that.$target = null;
        that.$targetValues = null;
        that.buildInputDropdown();
      }
      /**
       * 从远程加载筛选器选项列表
       * @param {Object} filter 筛选器对象
       * @param {function} callback 加载完后的回调
       */

    }, {
      key: "loadFilterRemoteItems",
      value: function loadFilterRemoteItems(filter, callback) {
        var _this2 = this;

        var store;
        var cache;
        var key = filter.key;

        if (typeof filter.remote.params === 'function') {
          key = JSON.stringify(filter.remote.params(this.data));
        } else if (filter.remote.params) {
          key = JSON.stringify(filter.remote.params);
        }

        store = this.store[filter.key];

        if (_typeof(store) !== 'object') {
          store = {
            cache: {}
          };
          this.store[filter.key] = store;
        }

        store.key = key;
        cache = store.cache[key];

        if (_typeof(cache) !== 'object') {
          cache = {
            loading: true,
            items: []
          };
          store.cache[key] = cache;
        }

        if (!cache.loading) {
          filter.items = cache.items;
          this.logger.log('[' + filter.key + '] use cached items');

          if (callback) {
            callback();
          }

          return;
        }

        if (Array.isArray(filter.remote)) {
          filter.items = [];
          var items = [];
          var load = 0;
          filter.remote.forEach(function (item) {
            _this2.fetchItems(item, function (data) {
              if (typeof item.converter === 'function') {
                items = items.concat(item.converter(data));
              } else {
                items = items.concat(data);
              }

              load += 1;

              if (load === filter.remote.length) {
                cache.items = items;
                filter.items = items;
                cache.loading = false;

                if (callback) {
                  callback();
                }
              }
            }, function () {
              store.loading = false;
            });
          });
        } else {
          this.fetchItems(filter.remote, function (data) {
            if (typeof filter.remote.converter === 'function') {
              cache.items = filter.remote.converter(data);
            } else {
              cache.items = data;
            }

            cache.loading = false;
            filter.items = cache.items;

            if (callback) {
              callback();
            }
          }, function () {
            store.loading = false;
          });
        }
      }
    }, {
      key: "fetchItems",
      value: function fetchItems(remoteData, successCallback, failCallback) {
        var options = {
          url: remoteData.url
        };

        if (typeof remoteData.params === 'function') {
          options.data = remoteData.params(this.data);
        } else if (remoteData.params) {
          options.data = remoteData.params;
        }

        var fetcher = function fetcher(ajaxOptions, onSuccess, onError) {
          return $.ajax(ajaxOptions).done(onSuccess).fail(onError);
        };

        if (typeof remoteData.fetcher === 'function') {
          fetcher = remoteData.fetcher;
        }

        fetcher(options, function (data) {
          successCallback(data);
        }, function () {
          failCallback();
        });
      }
    }, {
      key: "getCurrentFilterItem",
      value: function getCurrentFilterItem(value) {
        if (this.target && this.checkFilterItemsLoaded(this.target)) {
          return findFilterItemByValue(this.target, value);
        }

        return false;
      }
    }, {
      key: "execCommand",
      value: function execCommand(cmd) {
        return exec(this, cmd);
      }
    }, {
      key: "removeDateRangePicker",
      value: function removeDateRangePicker() {
        var picker = this.$input.data('daterangepicker');

        if (picker) {
          picker.remove();
        }

        this.$input.off('apply.daterangepicker cancel.daterangepicker');
      } // 应用当前输入的内容，更新元素

    }, {
      key: "applyInput",
      value: function applyInput() {
        this.logger.log('applyInput()', this.input);

        if (this.data.search) {
          this.data.search += this.input;
        } else {
          this.data.search = this.input;
        } // 没有目标筛选器的话，就当成更新搜索条件


        if (!this.target) {
          this.buildSearchTerm();
        }

        this.$input.val('');
        this.input = '';
      }
      /**
       * 设置是否禁用此组件
       * @param {Boolean} disabled 是否禁用
       */

    }, {
      key: "setDisabled",
      value: function setDisabled(disabled) {
        this.$input.prop('disabled', disabled);

        if (disabled) {
          this.active = false;
          this.$el.addClass('disabled');
          this.$inputDropdown.addClass('disabled').dropdown('hide');
        } else {
          this.$el.removeClass('disabled');
          this.$inputDropdown.removeClass('disabled');
        }
      }
    }, {
      key: "getFilterByName",
      value: function getFilterByName(name) {
        return findListItem(this.filters, function (f) {
          return f.name === name;
        });
      }
    }, {
      key: "getFilterByKey",
      value: function getFilterByKey(key) {
        return findListItem(this.filters, function (f) {
          return f.key === key;
        });
      }
      /**
       * 获取筛选器可用的选项列表
       * @param {Object} filter 筛选器对象
       */

    }, {
      key: "getFilterAvailableItems",
      value: function getFilterAvailableItems(filter) {
        var that = this;
        var selectedItems = that.data[filter.key];

        if (typeof selectedItems === 'undefined' || selectedItems.length < 1) {
          return filter.items;
        } // 如果是单选的筛选条件，只要选择一个选项，就不允许继续选了


        if (!filter.multiple) {
          return [];
        }

        if (_typeof(filter.none) === 'object') {
          if (selectedItems[0].toString() === filter.none.value.toString()) {
            return [];
          }
        }

        return filter.items.filter(function (item) {
          return !selectedItems.some(function (selected) {
            return selected.toString() === item.value.toString();
          });
        });
      }
      /**
       * 检查筛选器选项列表是否已经加载完毕
       * @param {Object} filter 筛选器对象
       */

    }, {
      key: "checkFilterItemsLoaded",
      value: function checkFilterItemsLoaded(filter) {
        var cache;
        var store = this.store[filter.key];

        if (_typeof(filter.remote) !== 'object') {
          return true;
        }

        if (_typeof(store) === 'object' && store.key) {
          cache = store.cache[store.key];

          if (_typeof(cache) === 'object' && !cache.loading) {
            return true;
          }
        }

        return false;
      } // 检查筛选器的值是否有效（排除“none”值）

    }, {
      key: "checkFilterValue",
      value: function checkFilterValue(filterKey) {
        var filter = this.getFilterByKey(filterKey);
        var value = this.data[filterKey];

        if (!value) {
          return false;
        }

        if (_typeof(filter.none) === 'object') {
          if (filter.multiple) {
            value = value[0];
          }

          if (value.toString() === filter.none.value.toString()) {
            return false;
          }
        }

        return true;
      } // 设置 tabs 选中

    }, {
      key: "setTabSelected",
      value: function setTabSelected() {
        for (var i = 0; i < this.target.tabs.length; i++) {
          if (this.$el.find(".item[data-tab=".concat(this.target.tabs[i].key, "]")).not('.filtered').length) {
            this.$el.find(".tab[data-tab=".concat(this.target.tabs[i].key, "]")).trigger('click');
            break;
          }
        }
      }
      /**
       * 检查筛选器是否可见
       * 筛选器是否可见，取决于是否有可选的选项、是否满足依赖
       * @param {Object} filter 筛选器对象
       */

    }, {
      key: "visibleFilter",
      value: function visibleFilter(filter) {
        var i;
        var value; // 如果这个筛选器依赖其它筛选器

        if (filter.requires) {
          if (typeof filter.requires === 'string') {
            if (!this.checkFilterValue(filter.requires)) {
              return false;
            }
          } else if (filter.requires instanceof Array) {
            for (i = 0; i < filter.requires.length; ++i) {
              if (!this.checkFilterValue(filter.requires[i])) {
                return false;
              }
            }
          }
        }

        if (filter.type === 'daterange') {
          return !this.data[filter.key];
        }

        if (this.checkFilterItemsLoaded(filter)) {
          if (this.getFilterAvailableItems(filter).length > 0) {
            return true;
          }

          if (_typeof(filter.none) === 'object') {
            value = this.data[filter.key];

            if (!value || value.length < 1) {
              return true;
            }

            if (filter.multiple) {
              return value.indexOf(filter.none.value.toString()) === -1;
            }

            return value !== filter.none.value.toString();
          }

          return false;
        }

        return true;
      }
    }, {
      key: "isEmpty",
      value: function isEmpty() {
        var that = this;
        var emptyFilters = that.filters.every(function (filter) {
          var value = that.data[filter.key];
          return typeof value === 'undefined' || value.length < 1;
        });
        return emptyFilters && !this.data.search && !this.input;
      } // 更新输入框内的占位符

    }, {
      key: "updatePlaceholder",
      value: function updatePlaceholder() {
        if (!this.isEmpty() || this.target) {
          this.$input.attr('placeholder', '');
        } else {
          this.$input.attr('placeholder', this.text.placeholder);
        }
      } // 更新“清除”按钮的状态

    }, {
      key: "updateClearButton",
      value: function updateClearButton() {
        if (!this.isEmpty() || this.target || this.$input.val()) {
          this.$el.removeClass('is-empty');
        } else {
          this.$el.addClass('is-empty');
        }
      }
    }, {
      key: "focus",
      value: function focus() {
        this.$input.focus();
      }
    }, {
      key: "hasFocus",
      value: function hasFocus() {
        if (this.active || this.$input.is(':focus')) {
          return true;
        }

        return false;
      }
      /**
       * 隐藏下拉菜单时的回调
       * @returns {Boolean} 是否需要隐藏
       */

    }, {
      key: "onHide",
      value: function onHide() {
        return !this.hasFocus();
      } // 锁定显示一段时间，避免下拉框自动隐藏

    }, {
      key: "lockShow",
      value: function lockShow() {
        var _this3 = this;

        this.active = true; // 功能与 deferFocusTime() 互斥，所以取消它的定时器

        if (this.timerForFocus) {
          clearTimeout(this.timerForFocus);
        }

        this.timerForFocus = setTimeout(function () {
          _this3.active = false;
        }, 300);
      } // 延长焦点状态时间，避免在做一些操作时因取消焦点而导致下拉框自动隐藏

    }, {
      key: "deferFocusTime",
      value: function deferFocusTime() {
        var _this4 = this;

        if (this.active && this.timerForFocus) {
          return;
        }

        if (this.timerForFocus) {
          clearTimeout(this.timerForFocus);
        }

        this.active = true;
        this.timerForFocus = setTimeout(function () {
          _this4.active = false; // 如果输入框还未获得焦点，则隐藏下拉框

          if (!_this4.$input.is(':focus')) {
            _this4.$inputDropdown.dropdown('hide');
          }
        }, 200);
      }
    }, {
      key: "resetInputPosition",
      value: function resetInputPosition() {
        var _this5 = this;

        // 如果已经在末尾，就不重复变动了，避免下拉框重复收起和展开
        if (this.$inputDropdown.is(':last-child')) {
          return;
        }

        setTimeout(function () {
          // 移动输入框到筛选器列表的末尾
          _this5.$tokens.append(_this5.$inputDropdown);
        }, 400);
      }
    }, {
      key: "addHistory",
      value: function addHistory(data) {
        this.history.add(this.filters, data);
        this.buildInputDropdown();
      }
    }, {
      key: "generateHistoryItem",
      value: function generateHistoryItem(data) {
        return this.history.generateItem(this.filters, data);
      }
    }, {
      key: "submit",
      value: function submit() {
        var that = this;
        var data = {};
        that.filters.forEach(function (f) {
          var value = that.data[f.key];

          if (typeof value !== 'undefined') {
            data[f.key] = that.data[f.key];
          }
        });
        that.applyInput();
        data.search = that.data.search;
        that.callback(cleanCopy(data));
      }
    }, {
      key: "updateState",
      value: function updateState() {
        this.updatePlaceholder();
        this.updateClearButton();
      } // 提交当前的改动

    }, {
      key: "commit",
      value: function commit() {
        this.updateState();
        this.$el.trigger('change.fsb', cleanCopy(this.data));
      } // 清除全部筛选器

    }, {
      key: "clear",
      value: function clear() {
        this.$el.find('.js-visual-token').remove();
        this.data = {};
        this.input = '';
        this.$input.val('');
        this.clearTargetFilter();
      } // 设置筛选器列表

    }, {
      key: "setFilters",
      value: function setFilters(filters) {
        var that = this;
        var keys = filters.map(function (f) {
          return f.key;
        });
        that.filters.forEach(function (f) {
          if (keys.indexOf(f.key) < 0) {
            that.removeFilter(f.key);
          }
        });
        that.clearTargetFilter();
        that.filters = filters;
        that.filters.forEach(function (f) {
          // 如果该筛选器使用的是远程的选项列表，且之前已经有缓存选项列表，则恢复它
          if (_typeof(f.remote) === 'object' && that.checkFilterItemsLoaded(f)) {
            that.loadFilterRemoteItems(f);
          }
        });
        that.buildInputDropdown();
      }
      /**
       * 设置选中的筛选条件
       * @param {Object} data 筛选条件数据
       */

    }, {
      key: "setData",
      value: function setData(data) {
        this.clear();
        this.data = data;
        this.buildFilterTokens();
        this.buildInputDropdown();
        this.buildSearchTerm();
        this.updateState();
      } // 销毁组件

    }, {
      key: "destroy",
      value: function destroy() {
        this.clear();
        this.$el.off('click');
        this.removeDateRangePicker();
        unbindEventHandlers();
      }
    }]);

    return FilteredSearchBox;
  }(Builder);

  function getSearchBoxValue($el, data) {
    var fullData = {};
    var searchBox = $el.data('filteredsearchbox');

    if (_typeof(data) !== 'object') {
      data = {};
    }

    ['search'].concat(_toConsumableArray(searchBox.filters.map(function (f) {
      return f.key;
    }))).forEach(function (key) {
      var value = data[key];

      if (typeof value === 'undefined') {
        fullData[key] = '';
      } else if (typeof value === 'number') {
        fullData[key] = value.toString();
      } else {
        fullData[key] = value;
      }
    });
    return cleanCopy(fullData);
  }

  function setSearchBoxValue($el, rawValue) {
    var searchBox = $el.data('filteredsearchbox');
    var currentValue = $el.data('value');

    if (!searchBox) {
      return;
    }

    var value = getSearchBoxValue($el, rawValue);

    if (compare(currentValue, value) !== 0) {
      $el.data('value', value);
      searchBox.setData(cleanCopy(value));
    }
  }
  /**
   * 筛选搜索框
   * @param {Object} props
   * @param {Object} props.config
   * @param {Object} props.config.history 历史记录，如果想禁用用该功能，可设置为 false
   * @param {number} props.config.history.limit 历史记录最大数量，默认值为 5，即：最多保存近 5 条搜索历史
   * @param {Object} props.config.history.store 存储库对象，默认值为 localStorage，如需自定义存储位置，请提供包含 setItem()、getItem()和 removeItem() 方法的对象
   * @param {string} props.config.history.storeKey 存储键值，默认值为 "GiteeSearchHistory"
   * @param {Array} props.config.filters 筛选器列表
   * @param {string} props.config.filters[].key 筛选器标识，例如："member_id"
   * @param {string} props.config.filters[].name 筛选器名称，例如："成员"
   * @param {string} props.config.filters[].type 筛选器类型，如需日期范围筛选，请指定为"daterange"
   * @param {string} props.config.filters[].icon 筛选器图标，例如："icon user"
   * @param {Object} props.config.filters[].none “无”条件，例如：{ name: "未关联", value: 0 }
   * @param {Object} props.value
   * @param {Function} props.onSubmit
   * @param {Function} props.onChange
   *
   * @see https://gitee.com/gitee-frontend/gitee-frontend
   * @see https://gitee.com/gitee-frontend/gitee-frontend/blob/master/src/components/filtered-search-box/main.vue
   */


  function ReactFilteredSearchBox(props) {
    var ref = react.useRef();
    react.useEffect(function () {
      var $el = $(ref.current);
      var searchBox = new FilteredSearchBox($el, props.config);

      searchBox.callback = function (data) {
        var value = getSearchBoxValue($el, data);

        if (props.onSubmit) {
          props.onSubmit(value, searchBox);
        }

        $el.data('value', value);
      };

      $el.on('change.fsb', function (e, data) {
        var value = getSearchBoxValue($el, data);
        var currentValue = $el.data('value');

        if (e.namespace === 'fsb' && compare(currentValue, value) !== 0) {
          $el.data('value', value);

          if (props.onChange) {
            props.onChange(cleanCopy(value), searchBox);
          }
        }
      });
      $el.data('filteredsearchbox', searchBox);
      $el.data('value', {});
      return function () {
        searchBox.destroy();
      };
    }, []);
    react.useEffect(function () {
      var searchBox = $(ref.current).data('filteredsearchbox');

      if (searchBox) {
        searchBox.setFilters(props.config.filters);
      }
    }, [props.config.filters]);
    react.useEffect(function () {
      setSearchBoxValue($(ref.current), props.value);
    }, [props.value]);
    return React.createElement('div', {
      ref: ref
    });
  }

  return ReactFilteredSearchBox;

}(React));
