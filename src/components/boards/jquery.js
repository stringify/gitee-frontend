import { Boards } from './main'
import { defaults } from './config'

$.fn.boards = function (options) {
  var that = this.data('boards')
  var settings = $.extend({}, $.fn.boards.settings, options)

  if (!that) {
    that = new Boards(this, settings)
    this.data('boards', that)
    that.load()
  }
  return this
}

$.fn.boards.settings = defaults
