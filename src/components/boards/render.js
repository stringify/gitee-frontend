export class Renderer {
  constructor(config) {
    this.config = config
  }

  getAvatarUrl(name, avatarUrl) {
    const Avatar = this.config.plugins.LetterAvatar
    if (!avatarUrl || avatarUrl.indexOf('no_portrait.png') === 0) {
      if (Avatar) {
        return Avatar(name)
      }
    }
    return avatarUrl
  }

  getCardId(issue) {
    if (this.config.getIusseId) {
      return this.config.getIusseId(issue)
    }
    return 'issue-card-' + issue.id
  }
}

export default null
